from flask import Flask                 #导入Flask主函数
from flask import render_template       #此函数可把Jinja2模版引擎集成到应有中
from flask_bootstrap import Bootstrap   #bootstrap是twitter的一个开源web框架
from flask_moment import Moment         #Moment是JavaScript的一个优秀客户端开源库
from datetime import datetime
from flask_wtf import  FlaskForm
from wtforms import StringField , SubmitField
from wtforms.validators import DataRequired
from flask import session,redirect,url_for,flash


app = Flask(__name__)
bootstrap = Bootstrap(app)
moment = Moment(app)
app.config['SECRET_KEY'] = 'hard to guess string'

class NameForm(FlaskForm):
    name = StringField('What is your name?', validators=[DataRequired()])
    submit= SubmitField('Submit')

@app.route('/', methods=['GET', 'POST'])
def index():
    form = NameForm()
    if form.validate_on_submit():
        old_name = session.get('name')
        if old_name is not None and old_name != form.name.data:
            flash('Looks like you have change your name!')
        session['name'] = form.name.data
        return redirect(url_for('index'))
    return render_template('index.html', form=form, name=session.get('name'))


@app.route('/user/<name>')
def user(name):
    return render_template('user.html',name=name)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'),404

@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'),500


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0',port=5000)